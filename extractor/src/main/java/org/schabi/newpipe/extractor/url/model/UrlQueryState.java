package org.schabi.newpipe.extractor.url.model;

public enum UrlQueryState {
    PRIVATE,
    PUBLIC,
    OTHER
}
