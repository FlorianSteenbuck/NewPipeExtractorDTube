package org.schabi.newpipe.extractor.url.model.params;

public class UrlPrivateParamsQuery extends UrlPseudoParamsQuery {
    public UrlPrivateParamsQuery(UrlParamsQuery query) {
        super(query);
    }
}
