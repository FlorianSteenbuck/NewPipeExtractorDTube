package org.schabi.newpipe.extractor.url.model.params;

public class UrlPublicParamsQuery<T> extends UrlPseudoParamsQuery {
    public UrlPublicParamsQuery(UrlParamsQuery query) {
        super(query);
    }
}
